ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

default_target: run

get_fuseki:
	@test -f apache-jena-fuseki-4.2.0.tar.gz || curl -O https://dlcdn.apache.org/jena/binaries/apache-jena-fuseki-4.2.0.tar.gz
	@tar -xf apache-jena-fuseki-4.2.0.tar.gz
	@rm apache-jena-fuseki-4.2.0.tar.gz

check_fuseki:
	@test -d apache-jena-fuseki-4.2.0 || (echo "fuseki not found aborted" && exit 1)

start_fuseki: check_fuseki
	@cd ./apache-jena-fuseki-4.2.0 && ./fuseki-server -v 

pip:
	@pip3 install -r requirements.txt

run: pip
	@python3 -m turtle_converter build upload -y

runLocal: pip
	@python3 -m turtle_converter --conf conf.local.yml build upload -y

runShare: pip
	@python3 -m turtle_converter --conf conf.share.yml build -y 

pipTesting: pip
	@pip3 install -r tests-requirements.txt

tests: pipTesting unittest show_reports

unittest:
	@python3 -m xmlrunner -v -o=reports

coverage:
	@rm -f .coverage
	@coverage run -m unittest

coverage_report: coverage
	@coverage report --include=turtle_converter/*.py
	@coverage xml --include=turtle_converter/*.py -o reports/coverageReport.xml
	@coverage html --include=turtle_converter/*.py -d reports/coverage

mutmut: coverage
	@rm -f .mutmut-cache
	-@mutmut run --use-coverage

mutmut_report: mutmut
	@mutmut junitxml >> reports/mutmutReport.xml
	@mutmut html
	@rm -rf reports/mutmut
	@mv html reports/mutmut

show_reports: coverage_report mutmut_report
	@echo "Coverage report generated at file://$(ROOT_DIR)/reports/coverage/index.html"
	@echo "Mutation report generated at file://$(ROOT_DIR)/reports/mutmut/index.html"

clearTests:
	@rm -rf reports