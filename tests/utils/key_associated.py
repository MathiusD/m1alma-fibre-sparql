from unittest import TestCase

from parameterized import parameterized
from turtle_converter.utils import key_associated


class TestUtilsKeyAssociated(TestCase):

    def makeMock(name: str = "Mock", *keyAssociated: str):
        return type("Mock", (key_associated.ClassWithKeyAssociated,), {
            "keyAssociated": lambda: keyAssociated
        })

    EmptyMock = makeMock('Empty')
    ExampleMock = makeMock('Example', 'Example')
    ExampleNotMock = makeMock('Example', 'Example', 'Not')

    @parameterized.expand([
        (EmptyMock, ()),
        (ExampleMock, ('Example', )),
        (ExampleNotMock, ('Example', 'Not')),
        (key_associated.ClassWithKeyAssociated(), None)
    ])
    def testKeyAssociated(self, classRelated: type, value: object):
        self.assertEqual(classRelated.keyAssociated(), value)

    @parameterized.expand([
        ([], {}),
        ([str], {}),
        ([EmptyMock], {}),
        ([ExampleMock], {'Example': [ExampleMock]}),
        ([ExampleMock, EmptyMock], {'Example': [ExampleMock]}),
        ([ExampleMock, str], {'Example': [ExampleMock]}),
        ([ExampleMock, ExampleNotMock], {'Example': [
         ExampleMock, ExampleNotMock], 'Not': [ExampleNotMock]}),
        ([ExampleMock, ExampleNotMock], {'Example': [
         ExampleMock, ExampleNotMock], 'Not': [ExampleNotMock]})
    ])
    def testMakeDict(self, selectors: list, values: dict):
        self.assertEqual(key_associated.makeKeyDict(selectors), values)
