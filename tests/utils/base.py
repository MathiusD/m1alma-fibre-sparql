from unittest import TestCase

from parameterized import parameterized
from turtle_converter.utils import base


class TestUtilsBase(TestCase):

    class Mock(base.BaseClass):
        pass

    class OtherMock(base.BaseClass):
        pass

    @parameterized.expand([
        (Mock(), {'class': Mock}),
        (OtherMock(), {'class': OtherMock})
    ])
    def testConstructorKwargs(self, obj: base.BaseClass, value: dict):
        self.assertEqual(obj.constructorKwargs, value)
