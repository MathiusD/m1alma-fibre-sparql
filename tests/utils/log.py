import logging
from unittest import TestCase
from unittest.mock import MagicMock, patch

from parameterized import parameterized
from turtle_converter.utils import getLogLevel, log


class TestUtilsLog(TestCase):

    @parameterized.expand([
        (True, 'Example'),
        (True, 'Any'),
        (False, 'Example')
    ])
    @patch('builtins.print')
    def testLog(self, lvlEnabled: bool, msg: str, mockedPrint):
        logging.root.isEnabledFor = MagicMock(return_value=lvlEnabled)
        logging.log = MagicMock()
        log(logging.DEBUG, msg)
        logging.root.isEnabledFor.assert_called_once_with(logging.DEBUG)
        if lvlEnabled:
            mockedPrint.assert_called_once_with(msg)
            logging.log.assert_called_once_with(logging.DEBUG, msg)
        else:
            logging.log.assert_not_called()
            mockedPrint.assert_not_called()

    @parameterized.expand([
        ('debug', logging.INFO, logging.DEBUG),
        ('DEBUG', logging.INFO, logging.DEBUG),
        ('deBug', logging.INFO, logging.DEBUG),
        (None, logging.INFO, logging.INFO),
        (1, logging.INFO, logging.INFO)
    ])
    def testGetLogLevel(self, lvl, default: int, expected):
        self.assertEqual(getLogLevel(lvl, default), expected)
