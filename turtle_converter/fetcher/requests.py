import logging

import requests

from ..utils import log, showPercentage, showSize
from .base import BaseFetcher


class HttpFetcher(BaseFetcher):

    @staticmethod
    def protocolsSupported():
        return [
            "http",
            "https"
        ]

    @classmethod
    def write(cls, uri: str, target: str, **kwargs):
        request = requests.get(uri, stream=True)
        if (request.status_code == 200):
            log(logging.INFO, "Dump %s from %s" % (target, uri))
            with open(target, 'wb') as file:
                totalLength = int(request.headers.get(
                    "content-length")) if "content-length" in request.headers.keys() else None
                beautifyLength = showSize(
                    totalLength) if totalLength else "Size not found"
                log(logging.INFO, "Start dl (%s)" % beautifyLength)
                chunkReceived = 0
                chunkSize = kwargs.get("chunkSize", 1024)
                for chunk in request.iter_content(chunk_size=chunkSize):
                    if chunk:
                        file.write(chunk)
                        file.flush()
                        chunkReceived += 1
                        lengthReceived = chunkReceived * chunkSize
                        if logging.root.level <= logging.INFO:
                            if not totalLength or totalLength < lengthReceived:
                                print("Receveid %s (%s chunk)\r" %
                                      (showSize(lengthReceived), chunkReceived), end="")
                            else:
                                showPercentage(
                                    lengthReceived, totalLength, "Reveived ",
                                    "$msg$percent%% (%s/%s) [%s chunk]" % (showSize(chunkReceived), beautifyLength, chunkReceived))

                if logging.root.level <= logging.INFO and not totalLength:
                    print("")
            return True
        else:
            log(logging.INFO, "Error on dump %s from %s\n(Code related : %s with reason : %s)" %
                (target, uri, request.status_code, request.reason))
            return False
