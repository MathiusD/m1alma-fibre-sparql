from enum import Enum


class FusekiEndpoint(Enum):
    UPDATE = "update"
    UPLOAD = "upload"
    GSPR = "gsp-r"
    GSPRW = "gsp-rw"
    QUERY = "query"


fusekiEndpointList = {
    FusekiEndpoint.UPDATE.value.lower(): FusekiEndpoint.UPDATE,
    FusekiEndpoint.UPLOAD.value.lower(): FusekiEndpoint.UPLOAD,
    FusekiEndpoint.GSPR.value.lower(): FusekiEndpoint.GSPR,
    FusekiEndpoint.GSPRW.value.lower(): FusekiEndpoint.GSPRW,
    FusekiEndpoint.QUERY.value.lower(): FusekiEndpoint.QUERY
}
