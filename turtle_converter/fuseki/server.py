import logging
import os

import requests
from requests_toolbelt import MultipartEncoder, MultipartEncoderMonitor

from ..utils import BaseClass, askBoolean, getRequireData, log, showSize, showPercentage
from .dataset import FusekiDataSet
from .dataset_types import FusekiDataSetType, fusekiDataSetTypeList
from .endpoints import FusekiEndpoint


class FusekiServer(BaseClass):

    def __init__(self, fusekiServerData: dict, loadServer: bool = True):
        self.baseUrl = getRequireData(
            "baseUrl", fusekiServerData, **self.constructorKwargs)
        self.dataSetName = getRequireData(
            "dataSetName", fusekiServerData, **self.constructorKwargs)
        self.username = fusekiServerData.get("username", None)
        self.password = fusekiServerData.get("password", None)
        self.clearBeforeUpdate = fusekiServerData.get(
            "clearBeforeUpdate", False)
        self.dataSetType = FusekiDataSetType.IN_MEMORY.value
        if "dataSetType" in fusekiServerData.keys() and fusekiServerData["dataSetType"].lower() in fusekiDataSetTypeList.keys():
            self.dataSetType = fusekiDataSetTypeList[fusekiServerData["dataSetType"].lower(
            )].value
        if loadServer:
            self.retrieveInfos()
        else:
            self.dataSetExist = False
            self.infos = None

    @property
    def fusekiUrl(self):
        return "%s/$/" % self.baseUrl

    @property
    def url(self):
        return "%s/%s/" % (self.baseUrl, self.dataSetName)

    @property
    def auth(self):
        return (self.username, self.password) if self.username and self.password else None

    def getInfos(self):
        uriTargeted = "%sdatasets/%s" % (self.fusekiUrl, self.dataSetName)
        log(logging.INFO, "Get infos from Fuseki Server (at %s)" % uriTargeted)
        res = requests.get(
            uriTargeted,
            auth=self.auth)
        log(logging.DEBUG, "%s - %s:\n%s" %
            (res.status_code, res.reason, res.text))
        data = None
        self.dataSetExist = True
        if res.ok:
            try:
                data = FusekiDataSet(res.json())
            except AttributeError:
                pass
        elif res.status_code == 404:
            self.dataSetExist = False
        return data

    def retrieveInfos(self):
        self.infos = self.getInfos()

    def checkInfos(self):
        if not self.infos:
            self.retrieveInfos()
        if not self.infos:
            raise Exception("Cannot get infos of dataset in Fuseki server")

    def removeDataSet(self, **kwargs):
        if not askBoolean("Operation can be canceled. Removing distant dataset at %s ?\n" % self.url, **kwargs):
            print("Aborting.")
            return False
        uriTargeted = "%sdatasets/%s" % (self.fusekiUrl, self.dataSetName)
        log(logging.INFO, "Remove dataset %s in Fuseki Server (at %s)" %
            (self.dataSetName, uriTargeted))
        res = requests.delete(
            uriTargeted,
            auth=self.auth)
        log(logging.DEBUG, "%s - %s:\n%s" %
            (res.status_code, res.reason, res.text))
        self.retrieveInfos()
        return res.ok

    def createDataSet(self, **kwargs):
        uriTargeted = "%sdatasets?dbType=%s&dbName=%s" % (
            self.fusekiUrl, self.dataSetType, self.dataSetName)
        log(logging.INFO, "Create dataset %s in Fuseki Server (at %s)" %
            (self.dataSetName, uriTargeted))
        res = requests.post(
            uriTargeted,
            auth=self.auth)
        log(logging.DEBUG, "%s - %s:\n%s" %
            (res.status_code, res.reason, res.text))
        self.retrieveInfos()
        return res.ok

    def sendTtl(self, pathOfTtl: str, **kwargs):
        if not askBoolean("File %s are sended to %s. Proceed ?\n" % (pathOfTtl, self.url), **kwargs):
            print("Aborting.")
            return False
        if not os.path.exists(pathOfTtl):
            raise AttributeError("Ttl file must be exist")
        self.checkInfos()
        if not FusekiEndpoint.UPLOAD in self.infos.services.keys():
            raise AttributeError("Upload endpoint must be present")
        uriTargeted = "%s%s" % (self.url,
                                self.infos.services[FusekiEndpoint.UPLOAD].endpoints[0])
        log(logging.INFO, "Send ttl to Fuseki Server (at %s)" % uriTargeted)
        totalLength = os.path.getsize(pathOfTtl)
        totalReadable = showSize(totalLength)

        def monitorCallback(monitor: MultipartEncoderMonitor):
            if logging.root.level <= logging.INFO:
                showPercentage(monitor.bytes_read, totalLength, "Uploaded of Ttl ",
                               "$msg$percent%% (%s/%s)" % (showSize(monitor.bytes_read), totalReadable))
        monitorFile = MultipartEncoderMonitor(MultipartEncoder(
            fields={'file': (pathOfTtl, open(pathOfTtl, 'rb'), 'text/turtle')}), monitorCallback)
        res = requests.post(
            uriTargeted,
            files={'file': monitorFile},
            headers={"Content-Type": monitorFile.content_type},
            auth=self.auth)
        log(logging.INFO, "%s - %s:\n%s" %
            (res.status_code, res.reason, res.text))
        return res.ok
