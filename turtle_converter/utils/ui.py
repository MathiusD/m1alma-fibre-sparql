from distutils.util import strtobool
from enum import Enum
import copy


def askBoolean(question: str, wrongValue: str = "Wrong value provided. (Type y or n for example)", **kwargs):
    if kwargs.get("autoConfirm", False):
        return True
    response = None
    while response is None:
        try:
            response = strtobool(input(question))
        except ValueError:
            print(wrongValue)
    return response


class Size(Enum):
    go = 1000000000
    mo = 1000000
    ko = 1000
    o = 1


sizeList = [
    Size.go,
    Size.mo,
    Size.ko,
    Size.o
]


def showSize(sizeProvided: int):
    for size in sizeList:
        div = sizeProvided / size.value
        if div >= 1:
            return "%s %s" % (div, size.name.lower())


def showPercentage(current: int, total: int, msg: str = None, pattern: str = "$msg$percent% ($current/$total)"):
    currentMsg = copy.copy(pattern)
    percent = 100 * (float(current) / float(total))
    currentMsg = currentMsg.replace("$msg", msg.__str__() if msg else "").replace(
        "$percent", int(percent).__str__()).replace("$current", current.__str__()).replace("$total", total.__str__())
    print("%s\r" % currentMsg, end="" if current < total else "\n")
