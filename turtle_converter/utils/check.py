def dataIsRequired(key: str, data: dict, msg: str = "'$key' must be defined in $className data", **kwargs):
    if data is None or not key in data.keys():
        raise AttributeError(
            msg.replace("$key", key).replace("$className",
                                             kwargs.get("class").__name__ if "class" in kwargs.keys() else ""))


def getRequireData(key: str, data: dict, msg: str = "'$key' must be defined in $className data", **kwargs):
    dataIsRequired(key, data, msg, **kwargs)
    return data[key]
