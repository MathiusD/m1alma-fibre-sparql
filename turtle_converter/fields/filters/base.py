import abc

from ...utils import ClassWithKeyAssociated


class BaseFilter(ClassWithKeyAssociated):

    @classmethod
    @abc.abstractmethod
    def isValid(cls, target, data, **kwargs):
        pass
