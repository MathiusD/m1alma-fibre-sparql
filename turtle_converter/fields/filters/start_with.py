from .base import BaseFilter


class StartWithFilter(BaseFilter):

    @classmethod
    def keyAssociated(cls):
        return [
            'startWith'
        ]

    @classmethod
    def isValid(cls, target, data, **kwargs):
        return type(target) == str and type(data) == str and data.upper().startswith(target.upper())
