from ..utils import BaseClass, getRequireData, log
from .instructions import getFromEntity


class LocatedField(BaseClass):

    def __init__(self, locatedFieldDict: dict):
        self.location = getRequireData(
            "location", locatedFieldDict, **self.constructorKwargs)

    def getFromEntity(self, entity: dict, **kwargs):
        return getFromEntity(entity, self.location, **kwargs)
