from ..utils import getRequireData
from .base import BaseField


class ConstantField(BaseField):

    def __init__(self, constantFieldData: dict):
        self.value = getRequireData(
            "value", constantFieldData, **self.constructorKwargs)
        super().__init__(constantFieldData)
