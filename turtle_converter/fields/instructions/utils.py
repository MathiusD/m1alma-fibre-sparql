import copy
from collections import OrderedDict

from .base import BaseInstruction


def getValueFromEntity(entity: dict, location: str):
    if not location:
        return None
    value = copy.deepcopy(entity)
    for key in location.split("/"):
        if type(value) in [dict, OrderedDict]:
            value = value.get(key, None)
        elif type(value) in [list, tuple]:
            try:
                value = value[int(key)] if len(value) > int(key) else None
            except ValueError:
                value = None
        else:
            value = None
        if value is None:
            break
    return value


def process(instructionName: str, last, current, instructions: list, **kwargs):
    if instructionName in instructions.keys():
        return instructions[instructionName][0].process(last, current, **kwargs)
    else:
        raise Exception(
            "Not instruction register for %s" % instructionName)


def getFromEntity(entity: dict, location: str or list, instructions: list, base: BaseInstruction, **kwargs):
    if type(location) is list:
        value = None
        for rawInstruction in location:
            if type(rawInstruction) == dict:
                if 'instruction' in rawInstruction.keys():
                    kwargsRelated = copy.deepcopy(kwargs)
                    for key in rawInstruction:
                        kwargsRelated[key] = rawInstruction[key]
                    value = process(rawInstruction.get('instruction'), value, getFromEntity(
                        entity, rawInstruction.get('location', None), instructions, base), instructions, **kwargsRelated)
                elif 'location' in rawInstruction.keys():
                    value = base.process(last, getFromEntity(
                        entity, rawInstruction, instructions, base))
            elif type(rawInstruction) == str:
                value = base.process(
                    last, getValueFromEntity(entity, rawInstruction))
        return value
    else:
        return getValueFromEntity(entity, location)
