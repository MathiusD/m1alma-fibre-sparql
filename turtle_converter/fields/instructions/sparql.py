import copy

from .base_requirements import BaseRequirementsInstruction
from .fetch import FetchInstruction


class SparqlInstruction(BaseRequirementsInstruction):

    @classmethod
    def keyAssociated(cls):
        return [
            'sparql'
        ]

    @staticmethod
    def requirements():
        return [
            'endpoint',
            'query'
        ]

    @classmethod
    def process(cls, last, current, **kwargs):
        if current and cls.respectRequirements(**kwargs):
            dedicatedKwargs = copy.deepcopy(kwargs)
            dedicatedKwargs['uri'] = '%s?query=%s&format=%s' % (
                kwargs.get('endpoint'), kwargs.get('query'), kwargs.get('format'))
            return FetchInstruction.process(last, current, **dedicatedKwargs)
        else:
            return None
