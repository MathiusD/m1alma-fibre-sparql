import copy

from .fetchable import Fetchable
from .fields import ConstantField, Field, RetainedField
from .prefix import Prefix
from .requirement import Requirement
from .utils import Extendable
from .turtle import Triple


class Targetable(Fetchable, Extendable):

    def __init__(self, **kwargs):
        self.blueprint = kwargs.get("blueprint", Extendable())
        self.name = self.getFromBlueprint("name", None, kwargs)
        self.url = self.getFromBlueprint("url", None, kwargs)
        self.outFormat = self.getFromBlueprint("outFormat", "ttl", kwargs)
        self.rawFormat = self.getFromBlueprint("rawFormat", "json", kwargs)
        self.inPath = self.getFromBlueprint("inPath", None, kwargs)
        self.loaderOpts = self.getFromBlueprint("loaderOpts", {}, kwargs)
        self.nameDataPrefix = self.getFromBlueprint(
            "nameDataPrefix", "", kwargs)
        self.commonPrefix = self.getFromBlueprint("commonPrefix", [], kwargs)
        self.prefix = copy.deepcopy(self.getAllFromBlueprint("prefix"))
        for prefix in kwargs.get("prefix", []):
            self.prefix.append(Prefix(prefix))
        self.fields = copy.deepcopy(self.getAllFromBlueprint("fields"))
        for field in kwargs.get("fields", []):
            self.fields.append(Field(field))
        self.retainedFields = copy.deepcopy(
            self.getAllFromBlueprint("retainedFields"))
        for retainedField in kwargs.get("retainedField", []):
            self.retainedFields.append(RetainedField(retainedField))
        self.constantFields = copy.deepcopy(
            self.getAllFromBlueprint("constantFields"))
        for constantField in kwargs.get("constantFields", []):
            self.constantFields.append(ConstantField(constantField))
        self.requirements = copy.deepcopy(
            self.getAllFromBlueprint("requirements"))
        for requirement in kwargs.get("requirements", []):
            self.requirements.append(Requirement(requirement))
        self.constantTriples = copy.deepcopy(
            self.getAllFromBlueprint("constantTriples"))
        for constantTriple in kwargs.get("constantsTriples", []):
            self.constantTriples.append(Triple.getFromDict(constantTriple))
        super().__init__(self.url, self.rawPath, self.rawFormat, self.requirements)

    def get(self, propertyName: str, default: object = None, extendable: dict = None):
        return super().get(propertyName, default, extendable, "blueprint")

    def getFromBlueprint(self, propertyName: str, default: object = None, extendable: dict = None):
        value = extendable.get(propertyName) if extendable else None
        if not value:
            if type(self.blueprint) is list:
                for ext in self.blueprint:
                    if not value:
                        value = ext.get(propertyName)
                        if value:
                            break
            else:
                value = self.blueprint.get(propertyName)
        return value if value else default

    def getAllFromBlueprint(self, propertyName: str):
        value = []
        if type(self.blueprint) is list:
            for ext in self.blueprint:
                for data in ext.get(propertyName, []):
                    value.append(data)
        else:
            for data in self.blueprint.get(propertyName, []):
                value.append(data)
        return value

    @property
    def allPrefix(self):
        allPrefix = []
        for prefix in self.prefix:
            allPrefix.append(prefix)
        for prefix in self.commonPrefix:
            allPrefix.append(prefix)
        return allPrefix

    @property
    def rawPath(self):
        return "%s.%s" % (self.name, self.rawFormat) if not self.inPath else self.inPath

    @property
    def outPath(self):
        return "%s.%s" % (self.name, self.outFormat)
