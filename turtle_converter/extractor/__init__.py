from ..utils import makeKeyDict
from .base import BaseExtractor
from .zip import ZipExtractor

_extractors = [
    ZipExtractor
]

extractors = makeKeyDict(_extractors)


def extract(path: str, target: str, rawFormat: str = None, defaultExtractor: BaseExtractor.__class__ = ZipExtractor, **kwargs):
    explicitFormat = rawFormat is not None
    if rawFormat is None:
        split = path.split(".")
        rawFormat = split[len(split) - 1]
    if rawFormat in extractors.keys():
        return extractors[rawFormat][0].extract(path, target)
    else:
        baseError = "No extractor register for '%s' format" % rawFormat
        if not explicitFormat and defaultExtractor:
            log(logging.INFO, "%s. Try with default extractor (%s)" %
                (baseError, defaultExtractor.__name__))
            try:
                return defaultExtractor.extract(path, target)
            except Exception:
                raise Exception(
                    "%s and defaultExtractor does'nt manage provided file." % baseError)
        else:
            raise Exception(baseError)


__all__ = [
    "extractors",
    "extract"
]
