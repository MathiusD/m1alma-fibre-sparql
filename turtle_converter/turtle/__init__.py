from .triple import TripleData, Triple
from .prefix import generatePrefix

__all__ = [
    "Triple",
    "TripleData",
    "generatePrefix"
]
