from ..utils import getRequireData


class TripleGen:

    def __init__(self, value: str, nbTriples: int):
        self.value = value
        self.nbTriples = nbTriples


class TripleData:

    def __init__(self, relation: str, value: str, typeValue: str = None, isLink: bool = False, isRDF: bool = False):
        self.relation = relation
        self.value = value
        self.typeValue = typeValue
        self.isLink = isLink
        self.isRDF = isRDF

    @property
    def partOfTriple(self):
        value = '%s%s%s%s' % ('' if self.isRDF else '<' if self.isLink else '"', self.value.__str__().replace("\\", "\\\\").replace("\"", "\\\""), '' if self.isRDF else '>' if self.isLink else '"',
                              "^^%s" % self.typeValue if self.typeValue and not self.isLink else "")
        return '%s %s' % (self.relation, value)

    def generateTriple(self, tripleName: str):
        return '%s %s .' % (tripleName, self.partOfTriple)

    @classmethod
    def getFromDict(cls, data: dict):
        return cls(
            getRequireData("relation", data, **{'class': cls}),
            getRequireData("value", data, **{'class': cls}),
            data.get('type', None), data.get('isLink', False),
            data.get('isRDF', False)
        )


class Triple:

    def __init__(self, name: str, data: list = []):
        self.name = name
        self.data = data

    def generateTriple(self):
        out = None
        used = 0
        for data in self.data:
            if type(data) == TripleData:
                if out is None:
                    out = "%s %s ." % (self.name, data.partOfTriple)
                else:
                    out = "%s;\n\t%s ." % (out[:-1], data.partOfTriple)
                used += 1
        return TripleGen(out, used)

    @classmethod
    def getFromDict(cls, data: dict):
        name = getRequireData("name", data, **{'class': cls})
        tripleData = []
        for entryData in data.get('data', None):
            entry = TripleData.getFromDict(entryData)
            if entry:
                tripleData.append(entry)
        return cls(name, tripleData)
