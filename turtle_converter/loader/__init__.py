import logging

from ..utils import log, makeKeyDict
from .base import BaseLoader
from .csv import CSVLoader
from .dbf import DBFLoader
from .json import JsonLoader
from .xlsx import XlsxLoader
from .yaml import YamlLoader

_loaders = [
    JsonLoader,
    DBFLoader,
    CSVLoader,
    YamlLoader,
    XlsxLoader
]

loaders = makeKeyDict(_loaders)


def load(path: str, rawFormat: str = None, defaultLoader: BaseLoader.__class__ = YamlLoader, **kwargs):
    explicitFormat = rawFormat is not None
    if rawFormat is None:
        split = path.split(".")
        rawFormat = split[len(split) - 1]
    if rawFormat in loaders.keys():
        return loaders[rawFormat][0].load(path, **kwargs)
    else:
        baseError = "No loader register for '%s' format" % rawFormat
        if not explicitFormat and defaultLoader:
            log(logging.INFO, "%s. Try with default loader (%s)" %
                (baseError, defaultLoader.__name__))
            try:
                return defaultLoader.load(path, **kwargs)
            except Exception:
                raise Exception(
                    "%s and defaultLoader does'nt manage provided file." % baseError)
        else:
            raise Exception(baseError)


__all__ = [
    "loaders",
    "load"
]
