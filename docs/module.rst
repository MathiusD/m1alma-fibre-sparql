.. currentmodule:: turtle_converter

GlobalModuleConfiguration
===========================

.. automodule:: turtle_converter
   :imported-members:
   :members:
   :undoc-members: